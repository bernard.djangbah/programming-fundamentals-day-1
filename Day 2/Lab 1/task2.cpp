#include <iostream>

int main () {
    int integer;
    int *integerptr {&integer};
    int& integerref {integer};
    int const constantint {10};

    integer = 5;

    std::cout << "Integer is " << integer << std::endl;
    std::cout << "Reference to integer is " << integerref << std::endl;
    std::cout << "Pointer to integer is " << integerptr << std::endl;

    *integerptr = integer + 1;
    integerref = integer + 1;
    std::cout << "Incremented integer is " << integer << std::endl;

    return 0;
}