#include <iostream>
#include <stdlib.h>

int main () {
    typedef std::string string_t;

    string_t str_arr[10];

    for (int i = 0; i < 10; i++){
        std::cin >> str_arr[i];
    }

    int maximum = 0;
    string_t largest;

    //Returns first largest found when the are other strings with same size
    for (auto word: str_arr){
        int size = word.length();
        if (size > maximum){
            maximum = size;
            largest = word;
        }
    }

    std::cout << largest;

    return 0;
}