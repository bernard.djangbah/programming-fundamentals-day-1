#include <iostream>

template <typename T>
void swap (T &a, T &b) {
    T temp = a;
    a = b;
    b = temp; 
}

int main () {
    char a {'a'};
    char b {'b'};

    swap(a, b);

    std::cout << a << b;

    return 0;
}