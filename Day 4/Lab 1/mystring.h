#include <iostream>

namespace my {
int strcmp(const char *l, const char *r){
        // Compares the elements of l with r and returns zero if they are all the same
        // a negative value if l < r and a positive value if l > r
        
        while (true)
        {
            // if characters reach the end of string and the loop hasn't been broken then they are the same
            if (*l == '\0' && *r == '\0') {
                return 0;
            }
            // if characters differ, or end of the second string is reached
            if (*l != *r) {
                break;
            }
    
            // move to the next pair of characters
            l++;
            r++;
        }

        if (*l < *r) {
            return 1;
        } else {
            return -1;
        }
    }

    int strlen (const char *s) {
        // Returns the length of s excluding the termination character '\0'
        int size = 0;

        while (s[size] != '\0')
            size++;

        return size;
    }

    char *strcat (char *l, const char *r) {
        // Concatenates (appends) r on to the end of l, and returns  l
        int size = my::strlen(l);
        char *head = l;
        
        head = head + size;
        
        while (*r != '\0'){
            *head = *r;
            head++;
            r++;
        }
        
        *head = '\0';

        return l;
    }

    char *strcpy(char *l, const char *r){
        // Copies the contents of r over the contents of l, and returns l
        char *head = l;

        int size = my::strlen(r);

        for (int i = 0; i < size; i++) {
            *head = *r;
            head++;
            r++;
        }
        *head = '\0';

        return l;
    }

    char *toupper(char *s){
        // Converts the characters in s to uppercase, and returns s
        char *head = s;
        int size = my::strlen(s);

        for (int i = 0; i < size; i++){
            if (*head >= 'a' && *head <= 'z') {
                *head -= 32;
            }
            head++;
        }
        return s;
    }
}