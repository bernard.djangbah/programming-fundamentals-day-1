#include <iostream>

int main () {
    std::string name = "Bernard";

    const char *ptr = name.c_str();

    int i = 0;
    
    while (ptr[i] != '\0'){
        std::cout << ptr[i++];
    }

    return 0;
}