#include <iostream>

int getNum () {
    int num;

    std::cout << "Please enter a number: ";
    std::cin >> num;

    return num;
}

int main () {
    int num;

    num = getNum();

    if (num % 2 == 0) {
        for (int count = 1; count <= 20; count++){
            std::cout << count << " x " << num << " = " << count * num << std::endl;
        }
    } else {
        int count = 0;
        int iter = 0;
        while (count < 31) {
            if (iter % 3 != 0){
                std::cout << iter << std::endl;
                count++;
            }
            iter++;
        }
    }

    return 0;
}