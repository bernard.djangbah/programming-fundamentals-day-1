#include <iostream>

// issues here to fix
void reverse (char line[]) {
    int size = 0;
    while (line[size] != '\0') {
        size++;
    }

    int j = size - 1;

    for (int i = 0; i < (size / 2); i++){
        while (i < j){
            // std::cout << "infinite loop";
            char temp = line[i];
            line[i] = line[j];
            line[j] = temp;
            j--;
        }
    }
}

int main () {
    char line[100];

    // std::cin.getline(line, 100);
    std::cin >> line;

    reverse(line);

    std::cout << line;

    return 0;
}