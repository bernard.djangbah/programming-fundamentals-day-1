#include <iostream>

int main () {
    int first, second;
    float result;
    char op;

    std::cout << "Please enter the first number: ";
    std::cin >> first;
    std::cout << "Please enter the first number: ";
    std::cin >> second;
    std::cout << "Which operation would you like to perform? (+, -, *, /): ";
    std::cin >> op;

    switch (op)
    {
    case '+':
        result = first + second;
        std::cout << "The result of your addition is " << result;
        break;
    case '-':
        result = first - second;
        std::cout << "The result of your substraction is " << result;
        break;
    case '*':
        result = first * second;
        std::cout << "The result of your multiplication is " << result;
        break;
    case '/':
        result = first / second;
        std::cout << "The result of your division is " << result;
        break;
    default:
        std::cout << "Invalid operator";
        break;
    }

    return 0;
}