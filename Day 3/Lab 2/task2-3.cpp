#include <iostream>

void output (int num) {
    std::cout << num << std::endl;
}

void output (float decimal) {
    std::cout << decimal << std::endl;
}

void output (long long_num) {
    std::cout << long_num << std::endl;
}

void output (std::string line) {
    std::cout << line << " -> std::string func here" << std::endl;
}

void output (char inp[]) {
    std::cout << inp << std::endl;
}

void output (char inp[], int start) {
    for (int i = 0; i < start; i++){
        std::cout << inp[i];
    }
    std::cout << std::endl;
}

void output (char inp[], int start, int end) {
    for ( int i = (start - 1); i < end; i++){
        std::cout << inp[start];
    }
    std::cout << std::endl;
}

// Write a collection of functions to output values of different types onto the display.
// All of the functions should have the same name, but be overloaded for int, 
// char, float, long and std::string. The functions should also output their type.

int main () {
    std::string myname = "Bernard";
    float num = 3.14;
    char word[] = "I dont know what to say";

    output(num);
    output(word, 10);
    output(myname);

    return 0;
}